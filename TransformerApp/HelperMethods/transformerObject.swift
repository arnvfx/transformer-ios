//
//  transformerObject.swift
//  TransformerApp
//
//  Created by Ali Raza Noorani on 2019-08-09.
//  Copyright © 2019 Ali Raza Noorani. All rights reserved.
//

import UIKit
import Realm
import RealmSwift
class transformerObject: Object {
    @objc  dynamic var name: String?
    @objc  dynamic var team: String?
    @objc  dynamic var teamLogo: String?
    @objc  dynamic var tID: String?
    @objc  dynamic var strenght: NSNumber? = 0
    @objc  dynamic var intelligence: NSNumber? = 0
    @objc  dynamic var speed: NSNumber? = 0
    @objc  dynamic var endurance: NSNumber? = 0
    @objc  dynamic var rank: NSNumber? = 0
    @objc  dynamic var courage: NSNumber? = 0
    @objc  dynamic var firePower: NSNumber? = 0
    @objc  dynamic var skill: NSNumber? = 0
    @objc  dynamic var overAllRaiting: NSNumber? = 0
    override class func primaryKey() -> String? {
        return "tID"
    }
}
