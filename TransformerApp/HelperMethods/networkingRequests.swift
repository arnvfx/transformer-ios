//
//  networkingRequests.swift
//  TransformerApp
//
//  Created by Ali Raza Noorani on 2019-08-09.
//  Copyright © 2019 Ali Raza Noorani. All rights reserved.
//

import UIKit
import Alamofire
import AFNetworking
import Realm
import RealmSwift
import SwiftyJSON
class networkingRequests: NSObject {
    let realm = try! Realm()
    //Making Post Request to save New Transformer
    func newTransformer(){
        
    }
    
    func getTokenCode(){
        
        
        
        let tokenApi = "https://transformers-api.firebaseapp.com/allspark";
        
        
        if let url = URL(string: tokenApi) {
            URLSession.shared.dataTask(with: url) { data, response, error in
                if let data = data {
                    if let jsonString = String(data: data, encoding: .utf8) {
                        print(jsonString)
                        
                        self.saveUserDefaults(key: SAVED_TOKEN_STRING,value: jsonString);
                        
                        
                        
                        
                        
                        
                        // let Auth_header    = [ "Authorization" : "token" ]
                        
                        
                        
                    }
                }
                }.resume()
        }
        
        
        
    }
    
    func saveUserDefaults(key:String, value:String){
        let defaults = UserDefaults.standard;
        defaults.set(value, forKey: key)
        defaults.synchronize();
        
        
        
        
        
        
        //  vc.myTableView.reloadData()
        
    }
    
    func createRealmObject(name:String,strenght:NSNumber,intelli:NSNumber,speed:NSNumber,rank:NSNumber,courage:NSNumber,firepower:NSNumber,skill:NSNumber,endurance:NSNumber,team:String,imageURL:String,tID:String){
        
        
        let transformer = transformerObject()
        transformer.name = name
        transformer.team = team
        transformer.strenght = strenght
        transformer.courage = courage
        transformer.speed = speed
        transformer.endurance = endurance
        transformer.firePower = firepower
        transformer.rank = rank
        transformer.skill = skill
        transformer.intelligence = intelli
        transformer.teamLogo = imageURL
        transformer.overAllRaiting = strenght.intValue + intelli.intValue + speed.intValue + endurance.intValue + firepower.intValue as NSNumber
        transformer.tID = tID
        
        try! realm.write {
            realm.add(transformer,update: true)
            
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
        
        
    }
    
    func addTransformer(TOKEN:String,transformer:transformerObject){
        let headers: HTTPHeaders = ["Authorization" : "Bearer "+TOKEN,
                                    "Content-Type": "application/json"]
        
        
        
        let params : Parameters = ["name":transformer.name,"strength":transformer.strenght,"intelligence":transformer.intelligence,"speed":transformer.speed,"endurance":transformer.endurance,"rank":transformer.rank,"courage":transformer.courage,"firepower":transformer.firePower,"skill":transformer.skill,"team":transformer.team]
        
        AF.request("https://transformers-api.firebaseapp.com/transformers", method: .post,parameters: params,encoding: JSONEncoding.default,headers: headers).responseJSON { response in
            let json = JSON(response.data)
            
            
            
            
            if let status = response.response?.statusCode {
                switch(status){
                case 201:
                    print(" success")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
                default:
                    print("error with response status: \(status)")
                }
            }
        }
        
        
    }
    
    func getTransformers(TOKEN:String){
        
        
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
        
        
        
        
        let headers: HTTPHeaders = ["Authorization" : "Bearer "+TOKEN,
                                    "Content-Type": "application/json"]
        AF.request("https://transformers-api.firebaseapp.com/transformers", method: .get,parameters: nil,encoding: JSONEncoding.default,headers: headers).responseJSON { response in
            
            // let resJson = JSON(response.data)
            
            //            let json = JSON(response.data)
            //            for (index, object) in json {
            //                let name = object["name"].stringValue
            //                print(name)
            //            }
            let json = JSON(response.data)
            print("Getting Objects: ",json)
            for (_, object) in json["transformers"] {
                
                let tName = object["name"].string
                
                let teamLogo = object["team_icon"].string
                let stength = object["strength"].number
                let speed = object["speed"].number
                let endurance = object["endurance"].number
                let rank = object["rank"].number
                let courage = object["courage"].number
                let firepower = object["firepower"].number
                let skill = object["skill"].number
                let team = object["team"].string
                let tID = object["id"].string
                let intelligence = object["intelligence"].number
                DispatchQueue.main.async {
                    
                    
                }
                
                
                
                
                
                
                
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200:
                        let objects = try! Realm().objects(transformerObject.self).filter("tID = %@", tID )
                        if(objects.count < 1){
                            
                            self.createRealmObject(name: tName!, strenght: stength!, intelli: intelligence!, speed: speed!, rank: rank!, courage: courage!, firepower: firepower!, skill: skill!, endurance: endurance!, team: team!, imageURL: teamLogo!, tID: tID!)
                        }else{
                            if let object = objects.first {
                                try! self.realm.write {
                                    let tObject = transformerObject()
                                    tObject.name = tName
                                    tObject.team = team
                                    tObject.strenght = stength
                                    tObject.courage = stength
                                    tObject.speed = speed
                                    tObject.endurance = endurance
                                    tObject.firePower = firepower
                                    tObject.rank = rank
                                    tObject.skill = skill
                                    tObject.intelligence = intelligence
                                    tObject.teamLogo = teamLogo
                                    tObject.tID = tID
                                    
                                    self.realm.add(tObject, update: true)
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
                                }
                                
                            }
                        }
                        
                        
                    default:
                        print("error with response status: \(status)")
                    }
                }
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
        }
    }
    
    func putTransformer(TOKEN:String,transformer:transformerObject){
        let headers: HTTPHeaders = ["Authorization" : "Bearer "+TOKEN,
                                    "Content-Type": "application/json"]
        
        
        let params : Parameters = ["id":transformer.tID,"name":transformer.name,"strength":transformer.strenght,"intelligence":transformer.intelligence,"speed":transformer.speed,"endurance":transformer.endurance,"rank":transformer.rank,"courage":transformer.courage,"firepower":transformer.firePower,"skill":transformer.skill,"team":transformer.team]
        
        
        
        
        let autobots = try! Realm().objects(transformerObject.self).filter("tID == %@",transformer.tID!)
        
        if let object = autobots.first {
            try! self.realm.write {
                let tObject = transformerObject()
                tObject.name = transformer.name
                tObject.team = transformer.team
                tObject.strenght = transformer.strenght
                tObject.courage = transformer.courage
                tObject.speed = transformer.speed
                tObject.endurance = transformer.endurance
                tObject.firePower = transformer.firePower
                tObject.rank = transformer.rank
                tObject.skill = transformer.skill
                tObject.intelligence = transformer.intelligence
                tObject.teamLogo = transformer.teamLogo
                tObject.tID = transformer.tID
                tObject.teamLogo = transformer.teamLogo
                realm.add(tObject, update: true)
            }
            AF.request("https://transformers-api.firebaseapp.com/transformers", method: .put,parameters: params,encoding: JSONEncoding.default,headers: headers).responseJSON { response in
                
                
                
                
                
                
                
                
                if let status = response.response?.statusCode {
                    switch(status){
                    case 201:
                        print(" success")
                    default:
                        print("error with response status: \(status)")
                    }
                }
            }
        }
        
        
    }
    func deleteTransformer(TOKEN:String,tID:String){
        
        
        
        let headers: HTTPHeaders = ["Authorization" : "Bearer "+TOKEN,
                                    "Content-Type": "application/json"]
        
        let link = API_ADD + "/" + tID
        AF.request(link, method: .delete,parameters: nil,encoding: JSONEncoding.default,headers: headers).responseJSON { response in
            
            
            if let status = response.response?.statusCode {
                switch(status){
                case 204:
                    print("Deleted successfully")
                    
                    self.getTransformers(TOKEN: TOKEN)
                default:
                    print("error with response status: \(status)")
                }
            }
            
            
            
        }
        
        
        
        
        
        
        
        
    }
    
    func createAlertView(message:String){
        let alertController = UIAlertController(title: "Hello", message: message, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        
    }
} 




