//
//  constants.swift
//  TransformerApp
//
//  Created by Ali Raza Noorani on 2019-08-09.
//  Copyright © 2019 Ali Raza Noorani. All rights reserved.
//

import UIKit
let SAVED_TOKEN_STRING: String = "token"
let API_ALLSPARK:String = "https://transformers-api.firebaseapp.com/allspark"
let API_ADD:String = "https://transformers-api.firebaseapp.com/transformers"
