//
//  TransformerListVC.swift
//  TransformerApp
//
//  Created by Ali Raza Noorani on 2019-08-11.
//  Copyright © 2019 Ali Raza Noorani. All rights reserved.
//

import UIKit
import Realm
import RealmSwift
import SwipeCellKit

class TransformerListVC: UIViewController,UITableViewDelegate, UITableViewDataSource,SwipeTableViewCellDelegate {
    let realm = try! Realm()
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let realmObject   = realm.objects(transformerObject.self)
        return realmObject.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath) as! listViewCell
        
        let cell: listViewCell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath as IndexPath) as! listViewCell
        cell.selectionStyle = .none
        cell.delegate = self
        let realmObject   = realm.objects(transformerObject.self)
        var object = transformerObject()
        object = realmObject[indexPath.row]
        cell.textName.text = object.name
        cell.textIntelligence.text = "Intelligence: " + object.intelligence!.stringValue
        cell.textStrenght.text = "Strength: " + object.strenght!.stringValue
        cell.testSpeed.text = "Speed: "+object.speed!.stringValue
        cell.textEndurance.text = "Endurance: "+object.endurance!.stringValue
        cell.firePower.text = "FirePower: "+object.firePower!.stringValue
        cell.textSkill.text = "Skill: "+object.skill!.stringValue
        cell.textRank.text = "Rank: "+object.rank!.stringValue
        cell.textCourage.text = "Courage: " + object.courage!.stringValue
        cell.textOverAllRaiting.text = "Over All Raiting: "+object.overAllRaiting!.stringValue
        
        cell.uiBackView.layer.cornerRadius = 8
        cell.uiBackView.layer.masksToBounds = true
        
        cell.uiBackView.layer.masksToBounds = false
      
        cell.uiBackView.layer.shadowColor = UIColor.black.cgColor
        cell.uiBackView.layer.shadowOpacity = 0.20
        cell.uiBackView.layer.shadowRadius = 4
        do {
            let imageURL = object.teamLogo!
            let data: Data = try! Data(contentsOf: URL(string:imageURL)!)
            cell.image_teamLogo.image = UIImage(data: data)
            
        }
        
        
        return cell
    }
    
    @IBOutlet weak var myTableView: UITableView!
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170;//Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let deleteAction = SwipeAction(style: .destructive, title: "Delete") { action, indexPath in
            // handle action by updating model with deletion
            let defaults = UserDefaults.standard;
            let value =  defaults.string(forKey: SAVED_TOKEN_STRING)
            let req = networkingRequests()
            let realmObject   = self.realm.objects(transformerObject.self)
            var object = transformerObject()
            object = realmObject[indexPath.row]
            req.deleteTransformer(TOKEN: value!, tID: object.tID!)
        }
        let edit = SwipeAction(style: .destructive, title: "Edit") { action, indexPath in
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddVC") as? ViewController
            vc?.index = indexPath.row
            vc?.isNew = false;
            self.navigationController?.pushViewController(vc!, animated: true)
            // handle action by updating model with deletion
        }
        // customize the action appearance
        edit.backgroundColor = UIColor.blue
        deleteAction.image = UIImage(named: "delete")
        
        return [deleteAction,edit]
    }
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addValue))
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "load"), object: nil)
        
        myTableView.allowsSelection = true
        myTableView.allowsMultipleSelectionDuringEditing = true
        myTableView.register(UINib(nibName: "listCell", bundle: nil), forCellReuseIdentifier: "LabelCell")
       
        myTableView.dataSource = self;
        myTableView.delegate = self;
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(doSomething), for: .valueChanged)
        
        // this is the replacement of implementing: "collectionView.addSubview(refreshControl)"
        myTableView.refreshControl = refreshControl
        let realmObject   = realm.objects(transformerObject.self)
        print(realmObject)
        // Do any additional setup after loading the view.
    }
    @objc func doSomething(refreshControl: UIRefreshControl) {
        let object = networkingRequests()
        let defaults = UserDefaults.standard;
        let value =  defaults.string(forKey: SAVED_TOKEN_STRING)
        object.getTransformers(TOKEN: value!)
        refreshControl.endRefreshing()
        realm.refresh()
        
    }
    @objc func addValue(){
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddVC") as? ViewController
        
        vc?.isNew = true;
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @objc func loadList(notification: NSNotification){
        self.myTableView.reloadData()
        
    }
    @IBAction func deleteAll(_ sender: Any) {
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
    }
    @IBAction func addNewTransformer(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddVC") as? ViewController
        
        vc?.isNew = true;
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @IBAction func createTestData(_ sender: Any) {
        self.createTestData()
    }
    
    
    func createTestData(){
        let object1 = transformerObject()
        object1.name =  "SoundWave"
        object1.strenght = 8
        object1.intelligence = 9
        object1.speed = 2
        object1.endurance = 6
        object1.rank = 7
        object1.courage = 5
        object1.firePower = 6
        object1.skill =  10
        object1.team = "D"
        
        
        let object2 = transformerObject()
        object2.name =  "BlueStrak"
        object2.strenght = 6
        object2.intelligence = 6
        object2.speed = 7
        object2.endurance = 9
        object2.rank = 5
        object2.courage = 2
        object2.firePower = 9
        object2.skill =  7
        object2.team = "A"
        
        
        let object3 = transformerObject()
        object3.name =  "HubCap"
        object3.strenght = 4
        object3.intelligence = 4
        object3.speed = 4
        object3.endurance = 4
        object3.rank = 4
        object3.courage = 4
        object3.firePower = 4
        object3.skill =  4
        object3.team = "A"
        
        
        let defaults = UserDefaults.standard;
        let value =  defaults.string(forKey: SAVED_TOKEN_STRING)
        var testData : Array<transformerObject> = Array()
        testData.append(object1)
        testData.append(object2)
        testData.append(object3)
        let req = networkingRequests()
        req.createAlertView(message: "Please Wait while data is being created..")
        DispatchQueue.global(qos: .utility).async {
            
            for object:transformerObject in testData {
                
                
                // req.addTransformer(TOKEN: value!, transformer: object)
                req.addTransformer(TOKEN: value!, transformer: object)
            }
            req.getTransformers(TOKEN: value!)
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }
}


