//
//  listViewCell.swift
//  TransformerApp
//
//  Created by Ali Raza Noorani on 2019-08-11.
//  Copyright © 2019 Ali Raza Noorani. All rights reserved.
//

import UIKit
import SwipeCellKit
class listViewCell: SwipeTableViewCell {

    @IBOutlet weak var testSpeed: UILabel!
    @IBOutlet weak var textIntelligence: UILabel!
    @IBOutlet weak var textStrenght: UILabel!
    @IBOutlet weak var textName: UILabel!
    @IBOutlet weak var image_teamLogo: UIImageView!
   
    @IBOutlet weak var textRank: UILabel!
    @IBOutlet weak var textEndurance: UILabel!
    @IBOutlet weak var firePower: UILabel!
   
    @IBOutlet weak var textCourage: UILabel!
    
    @IBOutlet weak var textSkill: UILabel!
    
    @IBOutlet weak var textOverAllRaiting: UILabel!
    
    @IBOutlet weak var uiBackView: UIView!
    
    
   
}
