//
//  ViewController.swift
//  TransformerApp
//
//  Created by Ali Raza Noorani on 2019-08-09.
//  Copyright © 2019 Ali Raza Noorani. All rights reserved.
//

import UIKit
import Realm
import RealmSwift
import Eureka

class ViewController: FormViewController{
    
    let realmObject   = try! Realm().objects(transformerObject.self)
    var name:String?
    var stenght:NSNumber?
    var intelligence:NSNumber?
    var speed:NSNumber?
    var endurance:NSNumber?
    var rank:NSNumber?
    var firePower:NSNumber?
    var skill:NSNumber?
    var courage:NSNumber?
    var team:String?
    var index:Int?
    var isNew:Bool?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addValue))
        
        form
            
            +++ Section(header: "Required Rule", footer: "Options: Validates on change")
            
            <<< TextRow()
                {
                    row in
                    row.title = "Tranformer Name"
                    row.value = isNew! ? "":realmObject[index!].name
                    
                    
                    row.tag = "name"
                }.onChange({ (row) in
                    self.name = row.value != nil ? row.value! : "" //updating the value on change
                })
            
            
            
            +++ Section(header: "Enter Numbers", footer: "Options: Validates on change after blurred")
            
            
            <<< IntRow() {
                $0.title = "Strenght"
                $0.value = isNew! ?0: realmObject[index!].strenght as? Int
                $0.add(rule: RuleRequired())
                $0.add(rule: RuleGreaterThan(min: Int(0)))
                $0.add(rule: RuleSmallerThan(max: Int(10)))
                $0.tag = "strenght"
                $0.validationOptions = .validatesOnChange
                }.onChange({ (row) in
                    self.stenght = row.value as NSNumber? //updating the value on change
                })
            
            
            
            
            <<< IntRow() {
                $0.title = "Intelligence"
                $0.value = isNew! ?0: realmObject[index!].intelligence as? Int
                $0.add(rule: RuleRequired())
                $0.add(rule: RuleGreaterThan(min: Int(0)))
                $0.add(rule: RuleSmallerThan(max: Int(10)))
                $0.tag = "intelligence"
                $0.validationOptions = .validatesOnChange
                }.onChange({ (row) in
                    self.intelligence = row.value as NSNumber? //updating the value on change
                })
            
            
            
            <<< IntRow() {
                $0.title = "Speed"
                $0.value = isNew! ?0: realmObject[index!].speed as? Int
                $0.add(rule: RuleRequired())
                $0.add(rule: RuleGreaterThan(min: Int(0)))
                $0.add(rule: RuleSmallerThan(max: Int(10)))
                $0.tag = "speed"
                $0.validationOptions = .validatesOnChange
                }
                
                .onChange({ (row) in
                    self.speed = row.value as NSNumber? //updating the value on change
                })
            
            
            
            
            <<< IntRow() {
                $0.title = "Endurance"
                $0.value = isNew! ?0: realmObject[index!].endurance as? Int
                $0.add(rule: RuleRequired())
                $0.add(rule: RuleGreaterThan(min: Int(0)))
                $0.add(rule: RuleSmallerThan(max: Int(10)))
                $0.tag = "endurance"
                $0.validationOptions = .validatesOnChange
                }
                
                .onChange({ (row) in
                    self.endurance = row.value as NSNumber? //updating the value on
                })
            
            
            
            <<< IntRow() {
                $0.title = "Rank"
                $0.value = isNew! ?0: realmObject[index!].rank as? Int
                $0.add(rule: RuleRequired())
                $0.add(rule: RuleGreaterThan(min: Int(0)))
                $0.add(rule: RuleSmallerThan(max: Int(10)))
                $0.tag = "rank"
                $0.validationOptions = .validatesOnChange
                }
                
                .onChange({ (row) in
                    self.rank = row.value as NSNumber? //updating the value on change
                })
            
            <<< IntRow() {
                $0.title = "Courage"
                $0.value = isNew! ?0: realmObject[index!].rank as? Int
                $0.add(rule: RuleRequired())
                $0.add(rule: RuleGreaterThan(min: Int(0)))
                $0.add(rule: RuleSmallerThan(max: Int(10)))
                $0.tag = "courage"
                $0.validationOptions = .validatesOnChange
                }
                
                .onChange({ (row) in
                    self.courage = row.value as NSNumber? //updating the value on change
                })
            
            <<< IntRow() {
                $0.title = "FirePower"
                $0.value = isNew! ?0: realmObject[index!].rank as? Int
                $0.add(rule: RuleRequired())
                $0.add(rule: RuleGreaterThan(min: Int(0)))
                $0.add(rule: RuleSmallerThan(max: Int(10)))
                $0.tag = "firePower"
                $0.validationOptions = .validatesOnChange
                }
                
                .onChange({ (row) in
                    self.firePower = row.value as NSNumber? //updating the value on change
                })
            
            
            <<< IntRow() {
                $0.title = "Skill"
                $0.tag = "skill"
                $0.value = isNew! ?0: realmObject[index!].rank as? Int
                $0.add(rule: RuleRequired())
                $0.add(rule: RuleGreaterThan(min: Int(0)))
                $0.add(rule: RuleSmallerThan(max: Int(10)))
                $0.validationOptions = .validatesOnChange
                }
                
                .onChange({ (row) in
                    self.skill = row.value as NSNumber? //updating the value on change
                })
            
            
            
            +++ Section(header: "Select an Option", footer: "Options: Validates on change after blurred")
            
            <<< PickerInputRow<String>{ row in
                row.title = "Pick A Team"
                row.tag = "team"
                
                let team = isNew! ?"": realmObject[index!].team
                if(team == "A"){
                    row.value = "Autobots"
                }else{
                    row.value = "Desepticons"
                }
                row.options = ["Autobots", "Desepticons"]
                
                }.onChange({ (row) in
                    self.team = row.value != nil ? row.value! : ""
                    if(row.value == "Autobots"){
                        self.team = "A"
                    }else{
                        self.team = "D"
                    }
                })
        
        
        // Do any additional setup after loading the view.
    }
    
    @objc func addValue(){
        
        
        let defaults = UserDefaults.standard;
        let value =  defaults.string(forKey: SAVED_TOKEN_STRING)
        let object = transformerObject()
        object.courage =  form.rowBy(tag: "courage")?.baseValue as? NSNumber
        object.skill =   form.rowBy(tag: "skill")?.baseValue as? NSNumber
        object.endurance =   form.rowBy(tag: "endurance")?.baseValue as? NSNumber
        object.firePower =   form.rowBy(tag: "firePower")?.baseValue as? NSNumber
        object.intelligence =  form.rowBy(tag: "intelligence")?.baseValue as? NSNumber
        object.name =   form.rowBy(tag: "name")?.baseValue as? String
        object.rank = form.rowBy(tag: "rank")?.baseValue as? NSNumber
        object.strenght =   form.rowBy(tag: "strenght")?.baseValue as? NSNumber
        object.speed =  form.rowBy(tag: "speed")?.baseValue as? NSNumber
        object.teamLogo = isNew! ?nil: realmObject[index!].teamLogo as? String
        if(form.rowBy(tag: "team")?.baseValue as? String == "Autobots"){
            object.team = "A"
        }else{
            object.team = "D"
        }
        
        object.tID =  isNew! ? nil:realmObject[index!].tID
        
        
        
        
        let req = networkingRequests()
        // req.addTransformer(TOKEN: value!, transformer: object)
        if(isNew!){
            var controller = ViewController()
            req.addTransformer(TOKEN: value!, transformer: object )
        }else{
            req.putTransformer(TOKEN: value!, transformer: object)
        }
        
        
    }
    @IBAction func getToken(_ sender: Any) {
        let myRequest = networkingRequests()
        let defaults = UserDefaults.standard;
        
        myRequest.getTransformers(TOKEN: defaults.string(forKey: SAVED_TOKEN_STRING) ?? "0")
        
        let realm = try! Realm()
        let realmObject = realm.objects(transformerObject.self)
        for Object:transformerObject in realmObject {
            
        }
        
        
    }
}

