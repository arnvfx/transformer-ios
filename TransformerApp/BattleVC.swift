//
//  BattleVC.swift
//  TransformerApp
//
//  Created by Ali Raza Noorani on 2019-08-11.
//  Copyright © 2019 Ali Raza Noorani. All rights reserved.
//

import UIKit
import Realm
import RealmSwift
class BattleVC: UIViewController {
    @IBOutlet weak var text_result: UILabel!
    @IBOutlet weak var textWinningTeam: UILabel!
    @IBOutlet weak var textLoosingTeam: UILabel!
    let realm = try! Realm()
    
    
    let autobots = try! Realm().objects(transformerObject.self).filter("team == 'A'").sorted(byKeyPath: "rank", ascending: false)
    let decepticon = try! Realm().objects(transformerObject.self).filter("team == 'D'").sorted(byKeyPath: "rank", ascending: false)
    let all = try! Realm().objects(transformerObject.self)
    var countryList : Array<transformerObject> = Array()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // let winner = self.battle()
        
        if(autobots.count < 1 || decepticon.count < 1){
            text_result.text = "Not Enough Players!"
        }else{
            let result = self.battle()
            if(result == "Decepticon Wins"){
                var text = ""
                for object:transformerObject in autobots {
                    text += "Loosing team contains| \(String(describing: object.name!))"
                    
                    
                }
                var wtext = ""
                for object:transformerObject in decepticon {
                    wtext += "Winning team contains|\(String(describing: object.name!))"
                    
                    
                }
                textWinningTeam.text = wtext
                textLoosingTeam.text = text
            }else if(self.battle() == "Autobots Wins"){
                var text = ""
                for object:transformerObject in decepticon {
                    text += "Loosing team contains|\(String(describing: object.name!))"
                    
                    
                }
                var wtext = ""
                for object:transformerObject in autobots {
                    wtext += "Winning team contains|\(String(describing: object.name!))"
                    
                    
                }
                textWinningTeam.text = wtext
                textLoosingTeam.text = text
            }
            text_result.text = result
        }
        
        
        // Do any additional setup after loading the view.
    }
    func battle() -> String{
        
        
        var matchCout = 0
        var autobotsWin = 0
        var decepticonWin = 0
        var isEveryOneDead = false
        var isGameBegan = false
        let req = networkingRequests()
        let defaults = UserDefaults.standard;
        let value =  defaults.string(forKey: SAVED_TOKEN_STRING)
        while matchCout < autobots.count {
            
            
            if(autobots.count > matchCout && decepticon.count > matchCout){
                isGameBegan = true
                
                
                let courageDiff = Int(truncating: autobots[matchCout].courage!) - Int(truncating: decepticon[matchCout].courage!)
                let strengthDiff = Int(truncating: autobots[matchCout].courage!) - Int(truncating: decepticon[matchCout].courage!)
                
                
                let skillDiff = Int(truncating: autobots[matchCout].skill!) - Int(truncating: decepticon[matchCout].skill!)
                
                
                if(autobots[matchCout].name == "Optimus Prime"){
                    autobotsWin += 1
                    try! realm.write {
                        realm.delete(decepticon[matchCout])
                        req.deleteTransformer(TOKEN: value!, tID: decepticon[matchCout].tID!)
                    }
                    
                    
                }else if(decepticon[matchCout].name == "Predaking"){
                    decepticonWin += 1
                    try! realm.write {
                        realm.delete(autobots[matchCout])
                        req.deleteTransformer(TOKEN: value!, tID: autobots[matchCout].tID!)
                    }
                    
                    
                }else if(autobots[matchCout].name == "Optimus Prime" && decepticon[matchCout].name == "Predaking"){
                    
                    isEveryOneDead = true;
                }
                    
                else if(Int(truncating: autobots[matchCout].courage!) > Int(truncating: decepticon[matchCout].courage!) && Int(courageDiff) >= 3 && abs(strengthDiff) >= 4){
                    // autobt has won
                    
                    try! realm.write {
                        realm.delete(decepticon[matchCout])
                        req.deleteTransformer(TOKEN: value!, tID: decepticon[matchCout].tID!)
                    }
                    autobotsWin += 1
                }else if(Int(truncating: autobots[matchCout].courage!) < Int(truncating: decepticon[matchCout].courage!) && Int(courageDiff) >= 3 && abs(strengthDiff) >= 4){
                    decepticonWin += 1
                    try! realm.write {
                        realm.delete(autobots[matchCout])
                        req.deleteTransformer(TOKEN: value!, tID: autobots[matchCout].tID!)
                    }
                    
                }else if(Int(truncating: autobots[matchCout].skill!) < Int(truncating: decepticon[matchCout].skill!) && abs(skillDiff) >= 3  ){
                    // decepticon win
                    decepticonWin += 1
                    try! realm.write {
                        realm.delete(autobots[matchCout])
                        req.deleteTransformer(TOKEN: value!, tID: autobots[matchCout].tID!)
                    }
                    
                }else if(Int(truncating: autobots[matchCout].skill!) > Int(truncating: decepticon[matchCout].skill!) && skillDiff >= 3 ){
                    // autobt win
                    try! realm.write {
                        realm.delete(decepticon[matchCout])
                        req.deleteTransformer(TOKEN: value!, tID: decepticon[matchCout].tID!)
                    }
                    autobotsWin += 1
                }else if(Int(truncating: autobots[matchCout].overAllRaiting!) > Int(truncating: decepticon[matchCout].overAllRaiting!)){
                    autobotsWin += 1
                    try! realm.write {
                        realm.delete(decepticon[matchCout])
                        req.deleteTransformer(TOKEN: value!, tID: decepticon[matchCout].tID!)
                    }
                    
                    
                }else if(Int(truncating: autobots[matchCout].overAllRaiting!) > Int(truncating: decepticon[matchCout].overAllRaiting!)){
                    autobotsWin += 1
                    try! realm.write {
                        realm.delete(decepticon[matchCout])
                        req.deleteTransformer(TOKEN: value!, tID: decepticon[matchCout].tID!)
                    }
                    
                    
                }
                    
                    
                    
                    
                    
                else if(Int(truncating: autobots[matchCout].overAllRaiting!) == Int(truncating: decepticon[matchCout].overAllRaiting!) ){
                    try! realm.write {
                        realm.delete(decepticon[matchCout])
                        realm.delete(autobots[matchCout])
                        req.deleteTransformer(TOKEN: value!, tID: decepticon[matchCout].tID!)
                        req.deleteTransformer(TOKEN: value!, tID: autobots[matchCout].tID!)
                    }
                    //It's A Tie
                    
                }
                
                
                
                
            }
            matchCout += 1
        }
        
        if(autobotsWin == decepticonWin){
            return "It's A Tie"
        }else if(isEveryOneDead){
            return "Everyone is dead"
        }
        else if(autobotsWin > decepticonWin){
            
            return "AutoBots Win"
        }else if(autobotsWin < decepticonWin){
            return "Decepticon Wins"
        }
        return "text"
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
